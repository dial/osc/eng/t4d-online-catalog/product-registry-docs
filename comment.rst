Commenting Features
===================

.. meta::
   :description lang=en:

Functionality has been developed for users of the Exchange to leave comments and feedback about
Use Cases, Building Blocks, Products, Organizations, and Projects. A user must have an account
and be logged in before they can leave comments. 

When viewing a specific Product, Organization, Building Block, or Project, the user will find 
a section for comments at the bottom of the page. The user can add a comment or reply to an existiing
comment. A user can also upvote a comment. 

Please note that all comments are moderated and inappropriate comments will be removed.
