Projects
========

.. meta::
   :description lang=en:

Projects show where products have been implemented. The Exchange has several sources of data
for product implementations including the Digital Health Atlas developed by WHO.

A project contains information about what products were used, what organizations were 
involved in the project, and what the country context is for the project. The project
also includes a description of the work and timeline. 

The projects information can be found either by navigating to the `projects page`_ in the 
Exchange or by clicking on the `maps tab`_ at the top of the Exchange. The project map shows how many
projects have been implemented in specific countries. Click on a circle for a country to see a list of 
projects implemented in this country context. The user can then select a specific
project to see more information about it.

Expand the filter section to filter projects by sector or tags.

.. _`projects page`: https://registry.dial.community/projects