scipy==1.11.1
numpy==1.25.2
pandas==2.0.3
matplotlib==3.7.2
sphinx-rtd-theme==1.2.2