SDG Framework
=============

.. meta::
   :description lang=en:

The SDG Digital Investment Framework was developed by DIAL and ITU in 2018. The Framework
advocates for using a 'Building Blocks' approach to using digital technologies for development. 
Many Projects require common types of functionality, such as Identification, Data Collection, 
Messaging, and Payments. Rather than creating bespoke software solutions for each project, the
Framework guides actors on how to identify and use reusable, standards-based components to 
provide core functionalities. This reduces duplication of work, encourages interoperability,
and allows organizations to more quickly develop and deploy technology solutions. 

A description of each of the core elements of the SDG Framework can be found at the following
links:

 * :doc:`Sustainable Development Goals <sdg>`
 * :doc:`Use Cases <use_case>`
 * :doc:`Workflows <workflow>`
 * :doc:`Building Blocks <building_block>`
 * :doc:`Products <product>`